import 'package:flutter/material.dart';
import 'package:katlego_duiker/dashboard.dart';

class EditProfile extends StatelessWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Edit Profile", style: TextStyle(fontSize: 15)),
        centerTitle: true,
      ),
      body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
//SizeBox
            const SizedBox(
              height: 15,
            ),
//Text 1
            const Text(
              "Account Information",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
//SizeBox 1
            const SizedBox(
              height: 15,
            ),
//Text Field 1
            const Padding(
              padding: EdgeInsets.all(20.0),
              child: TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Edit Name',
                  )),
            ),
//Text Field 2
            const Padding(
              padding: EdgeInsets.all(20.0),
              child: TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Change Email',
                  )),
            ),
//Text Field 3
            const Padding(
              padding: EdgeInsets.all(20.0),
              child: TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Change Password',
                  )),
            ),
//SizeBox 2
            const SizedBox(
              height: 15,
            ),
//Button 1
            ElevatedButton(
              onPressed: () => {
                Navigator.pop(
                  context,
                  MaterialPageRoute(builder: (context) => const Dashboard()),
                ),
              },
              child: const Text("Save Changes"),
            ),
          ])),
    );
  }
}
