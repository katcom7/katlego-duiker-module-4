import 'package:flutter/material.dart';

class FeaturePage2 extends StatelessWidget {
  const FeaturePage2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Feature Page 2", style: TextStyle(fontSize: 15)),
           centerTitle: true,
        ),
        body: const Center(
          child: Text('Welcome to Feature Page 2'),
        ));
  }
}
