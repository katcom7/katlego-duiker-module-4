import 'package:flutter/material.dart';
import 'package:katlego_duiker/signUp.dart';
import 'package:katlego_duiker/dashboard.dart';

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Login", style: TextStyle(fontSize: 15)),
        centerTitle: true,
      ),
      body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
            Stack(
              clipBehavior: Clip.none,
              children: const [
                CircleAvatar(
                  backgroundImage: AssetImage('assets/Katlego.png'),
                )
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            const Text(
              "Hello, User",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
//Text Field 1
            const Padding(
              padding: EdgeInsets.all(20.0),
              child: TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email or Username',
                  )),
            ),
//Text Field 2
            const Padding(
              padding: EdgeInsets.all(20.0),
              child: TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                  )),
            ),
//SizeBox 1
            const SizedBox(
              height: 15,
            ),
//Button 1
            ElevatedButton(
              onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const Dashboard()),
                ),
              },
              child: const Text("Login"),
            ),
//SizeBox 2
            const SizedBox(
              height: 10,
            ),

            const Text("or Signup below"),
//SizeBox 3
            const SizedBox(
              height: 10,
            ),
//Button 2
            ElevatedButton(
              onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const SignUp()),
                ),
              },
              child: const Text("SignUp"),
            ),
          ])),
    );
  }
}
