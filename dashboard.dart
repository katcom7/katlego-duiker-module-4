import 'package:flutter/material.dart';
import 'package:katlego_duiker/editProfile.dart';
import 'package:katlego_duiker/featurePage1.dart';
import 'package:katlego_duiker/featurePage2.dart';
import 'package:katlego_duiker/login.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Dashboard", style: TextStyle(fontSize: 15)),
        centerTitle: true,
      ),
      body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
//Text 1
            const Text(
              "Welcome to the App",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
//SizeBox 1
            const SizedBox(
              height: 15,
            ),
//Button 1
            ElevatedButton(
              onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const FeaturePage1()),
                ),
              },
              child: const Text("Feature Page 1"),
            ),
//SizeBox 3
            const SizedBox(
              height: 15,
            ),
//Button 2
            ElevatedButton(
              onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const FeaturePage2()),
                ),
              },
              child: const Text("Feature Page 2"),
            ),
//SizeBox 4
            const SizedBox(
              height: 15,
            ),
//Button 3
            ElevatedButton(
              onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const EditProfile()),
                ),
              },
              child: const Text("Edit Profile"),
            ),
          ])),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pop(
            context,
            MaterialPageRoute(builder: (context) => const Login()),
          );
        },
        child: const Text("logout", style: TextStyle(fontSize: 12)),
      ),
    );
  }
}
