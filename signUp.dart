import 'package:flutter/material.dart';
import 'package:katlego_duiker/dashboard.dart';

class SignUp extends StatelessWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("SignUp", style: TextStyle(fontSize: 15)),
        centerTitle: true,
      ),
      body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
            //Text
//Text 1
            const Text(
              "Register a New Account",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
//SizeBox 1
            const SizedBox(
              height: 15,
            ),
//Text Field 1
            const Padding(
              padding: EdgeInsets.all(20.0),
              child: TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Name',
                  )),
            ),
//Text Field 2
            const Padding(
              padding: EdgeInsets.all(20.0),
              child: TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Username',
                  )),
            ),
//Text Field 3
            const Padding(
              padding: EdgeInsets.all(20.0),
              child: TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email',
                  )),
            ),
//Text Field 4
            const Padding(
              padding: EdgeInsets.all(20.0),
              child: TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Create Password',
                  )),
            ),
//Text Field 5
            const Padding(
              padding: EdgeInsets.all(20.0),
              child: TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Repeat Password',
                  )),
            ),
//SizeBox
            const SizedBox(
              height: 15,
            ),
//Button 1
            ElevatedButton(
              onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const Dashboard()),
                ),
              },
              child: const Text("Register Account"),
            ),
          ])),
    );
  }
}
